class PostsController < ApplicationController
  def index
    @posts = Post.all
    respond_to do |format|
      format.html
      format.json { render :json => @posts }
    end
  end

  def create
    @post = Post.new(post_params)
    respond_to do |format|
      format.json do
        if @post.save
          render :json => @post
        else
          render :json => { :errors => @post.errors.messages }, :status => 422
        end
      end
    end
  end

  def update
    @post = Post.find(params[:id])
    respond_to do |format|
      format.json do
        if @post.update(post_params)
          render :json => @post
        else
          render :json => { :errors => @post.errors.messages }, :status => 422
        end
      end
    end
  end

  def destroy
    Post.find(params[:id]).destroy
    respond_to do |format|
      format.json { render :json => {}, :status => :no_content }
    end
  end

  private

  def post_params
    params.require(:post).permit(:title, :name, :content)
  end
end
