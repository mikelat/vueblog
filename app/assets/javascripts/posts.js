var posts = new Vue({
  el: '#posts',
  data: {
    posts: [],
      post: {
      title: '',
      name: '',
      content: ''
    },
    errors: {}
  },
  mounted: function() {
    var that = this;
    $.get({
      url: '/posts.json',
      success: function(res) {
        that.posts = res;
      }
    });
  },
  methods: {
    newPost: function () {
      var that = this;
      $.post({
        data: {
          post: that.post,
        },
        url: '/posts.json',
        success: function(res) {
          that.errors = {}
          that.posts.push(res);
        },
        error: function(res) {
          that.posts = res.responseJSON.errors
        }
      });
    }
  }
});

Vue.component('post-row', {
  data: function () {
    return {
      editMode: false,
      errors: {}
    }
  },
  methods: {
    updatePost: function () {
      var that = this;
      $.ajax({
        method: 'PUT',
        data: {
          post: that.post,
        },
        url: '/posts/' + that.post.id + '.json',
        success: function(res) {
          that.errors = {};
          that.post = res;
          that.editMode = false;
        },
        error: function(res) {
          that.errors = res.responseJSON.errors
        }
      });
    },
    deletePost: function () {
      var that = this;
      if(confirm("are you sure you want to delete this?")) {
        $.ajax({
          method: 'DELETE',
          url: '/posts/' + that.post.id + '.json',
          success: function(res) {
            that.$el.remove();
          }
        });
      }
    }
  },
  template: '#post-row',
  props: {
    post: Object
  }
})